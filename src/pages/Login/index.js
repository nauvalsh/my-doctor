import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ILLogo} from '../../assets';
import {Button, Gap, Input, Link} from '../../components';

export default function Login() {
  return (
    <View style={styles.container}>
      <ILLogo />
      <Gap height={40} />
      <Text style={styles.title}>Masuk dan mulai berkonsultasi</Text>
      <Gap height={40} />
      <Input label="Email Address" />
      <Gap height={24} />
      <Input label="Password" />
      <Gap height={10} />
      <Link label="Forgot My Password" />
      <Gap height={40} />
      <Button type="primary" title="Sign In" />
      <Gap height={30} />
      <Link label="Create New Account" size={16} align="center" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 40,
    backgroundColor: 'white',
    flex: 1,
  },
  title: {
    fontSize: 20,
    fontWeight: '600',
    maxWidth: 160,
    fontFamily: 'Nunito-SemiBold',
  },
});
