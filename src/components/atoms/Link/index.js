import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default function Link({label, size, align}) {
  return (
    <View>
      <Text style={[styles.label(size, align)]}>{label}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  label: (size, align) => ({
    fontFamily: 'Nunito-semiBold',
    fontSize: size,
    textDecorationLine: 'underline',
    color: '#7D8797',
    textAlign: align,
  }),
});
