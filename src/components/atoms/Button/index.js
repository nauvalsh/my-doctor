import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';

export default function Button({type, title, onPress}) {
  return (
    <TouchableOpacity style={styles.wrapper(type)} onPress={onPress}>
      <Text style={styles.title(type)}>{title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  wrapper: (type) => ({
    paddingVertical: 10,
    borderRadius: 10,
    backgroundColor: type === 'secondary' ? 'white' : '#0BCAD4',
  }),
  title: (type) => ({
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Nunito-SemiBold',
    color: type === 'secondary' ? '#112340' : 'white',
  }),
});
